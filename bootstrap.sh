#!/bin/bash

# Test environment
if [[ ! -d /builds ]]; then
	echo "In order to use this image, you must mount the working directory to /builds"
	exit 1
fi
REFERENCE=$(ls -d /builds/* | head -1)
if [[ -z "${REFERENCE}" ]]; then
	echo "Aborting because of empty working directory"
	exit 1
fi

# Prepare environment
_duniter_prepare() {
	touch /duniter/reference || exit 1
	chown --reference "${REFERENCE}" /duniter/reference || exit 1
	chown -R duniter:duniter /builds/* || exit 1
}

# Terminate
_duniter_terminate() {
	chown -R --reference /duniter/reference /builds/* || exit 1
	rm -f /duniter/reference
	exit ${1}
}

# Prepare and execute building
_duniter_prepare
su - duniter
_duniter_terminate $?
